#!/bin/bash

namespace=$1
helm_release=$2

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}


try
(  
    [[ $(helm uninstall $helm_release) ]]
    echo " api-resources 1"
    [[ $(kubectl api-resources --namespaced=false | awk '{print $1}' | xargs -n 1 kubectl delete -l app.kubernetes.io/instance=$helm_release) ]]
    echo " api-resources 2"
    [[ $(kubectl api-resources --namespaced=false | awk '{print $1}' | xargs -n 1 kubectl delete -l release=$helm_release) ]]
    echo " delete all"
    [[ $(kubectl delete all -l release=$helm_release --all-namespaces) ]]
    
    echo "######################################################"
    echo "## PROMETHEUS OPERATOR: safety-delete.sh: well done ##"
    echo "######################################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 


