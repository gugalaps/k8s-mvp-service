#!/bin/bash

helm_release=$1
namespace=$2
balancerIP=$3
prometheus_operator_version=$4

alermanager_svc_name=$5
grafana_svc_name=$6
prometheus_svc_name=$7

helm_target_release=$8


echo "
helm_release: $helm_release
namespace: $namespace
alertmanager_host: alertmanager.$balancerIP.nip.io
alermanager_svc_name: $alermanager_svc_name
grafana_host: pgrafana.$balancerIP.nip.io
grafana_svc_name: $grafana_svc_name
prometheus_host: po.$balancerIP.nip.io
prometheus_svc_name: $prometheus_svc_name
prometheus_operator_version: $prometheus_operator_version
helm_target_release: $helm_target_release
">./prometheus-operator/po-helm/values.yaml

chmod 775 ./prometheus-operator/po-helm/values.yaml