#!/bin/bash

slack_api_url=$1
slack_channel=$2
prometheus_operator_version=$3
prometheus_operator_helm_release=$4


index='$index'
label='$label'
labels='$labels'
one='$1'

echo "global:
  resolve_timeout: 1m
  slack_api_url: '$slack_api_url'

route:
  receiver: 'slack-notifications'

receivers:
- name: 'slack-notifications'
  slack_configs:
  - channel: '#$slack_channel'
    send_resolved: true
    icon_url: https://avatars3.githubusercontent.com/u/3380462
    title: |-
      [{{ .Status | toUpper }}{{ if eq .Status \"firing\" }}:{{ .Alerts.Firing | len }}{{ end }}] {{ .CommonLabels.alertname }} for {{ .CommonLabels.job }}
      {{- if gt (len .CommonLabels) (len .GroupLabels) -}}
        {{\" \"}}(
        {{- with .CommonLabels.Remove .GroupLabels.Names }}
          {{- range $index, $label := .SortedPairs -}}
            {{ if $index }}, {{ end }}
            {{- $label.Name }}=\"{{ $label.Value -}}\"
          {{- end }}
        {{- end -}}
        )
      {{- end }}
    text: >-
      {{ range .Alerts -}}
      *Alert:* {{ .Annotations.title }}{{ if .Labels.severity }} - \`{{ .Labels.severity }}\`{{ end }}

      *Description:* {{ .Annotations.description }}

      *Details:*
        {{ range .Labels.SortedPairs }} • *{{ .Name }}:* \`{{ .Value }}\`
        {{ end }}
      {{ end }}">./prometheus-operator/alertmanager/alertmanager.yaml

chmod 775 ./prometheus-operator/alertmanager/alertmanager.yaml


echo "apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    app: prometheus-operator
    chart: prometheus-operator-$prometheus_operator_version
    heritage: Helm
    release: $prometheus_operator_helm_release
  name: $prometheus_operator_helm_release-prometheus-operator-alertmanager.rules
  namespace: observability
spec:
  groups:
  - name: alertmanager.rules
    rules:
    - alert: AlertmanagerConfigInconsistent
      annotations:
        message: The configuration of the instances of the Alertmanager cluster \`{{$labels.service}}\`
          are out of sync.
      expr: count_values(\"config_hash\", alertmanager_config_hash{job=\"$prometheus_operator_helm_release-prometheus-operator-alertmanager\",namespace=\"observability\"})
        BY (service) / ON(service) GROUP_LEFT() label_replace(max(prometheus_operator_spec_replicas{job=\"$prometheus_operator_helm_release-prometheus-operator-operator\",namespace=\"observability\",controller=\"alertmanager\"})
        by (name, job, namespace, controller), \"service\", \"$one\", \"name\", \"(.*)\") != 1
      for: 5m
      labels:
        severity: critical
    - alert: AlertmanagerFailedReload
      annotations:
        message: Reloading Alertmanager's configuration has failed for {{ $labels.namespace
          }}/{{ $labels.pod}}.
      expr: alertmanager_config_last_reload_successful{job=\"$prometheus_operator_helm_release-prometheus-operator-alertmanager\",namespace=\"observability\"} == 0
      for: 10m
      labels:
        severity: warning
    - alert: AlertmanagerMembersInconsistent
      annotations:
        message: Alertmanager has not found all other members of the cluster.
      expr: |-
        alertmanager_cluster_members{job=\"$prometheus_operator_helm_release-prometheus-operator-alertmanager\",namespace=\"observability\"}
          != on (service) GROUP_LEFT()
        count by (service) (alertmanager_cluster_members{job=\"$prometheus_operator_helm_release-prometheus-operator-alertmanager\",namespace=\"observability\"})
      for: 5m
      labels:
        severity: critical
    - alert: ExampleAlert
      expr: vector(1)      
    - alert: ExampleAlert2
      expr: vector(1) 
    - alert: ExampleAlert3
      expr: vector(4)
    - alert: ExampleAlert4
      expr: vector(1)  
">./prometheus-operator/alertmanager/prometheus.rule.yaml
chmod 775 ./prometheus-operator/alertmanager/prometheus.rule.yaml