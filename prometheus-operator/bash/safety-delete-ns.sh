#!/bin/bash

namespace=$1

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}


try
(  
    echo "namespace"
    [[ $(kubectl delete --cascade namespace $namespace) ]]
    echo "######################################################"
    echo "## PROMETHEUS OPERATOR: safety-delete-ns.sh: well done ##"
    echo "######################################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 


