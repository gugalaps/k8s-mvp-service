#!/bin/bash

namespace=$1
git_app=$2
git_app_branch=$3
git_app_path=$4

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}


echo "
git:
  url: $git_app
  path: $git_app_path
  ref: $git_app_branch
  ciSkip: true
  pollInterval: 1m
  secretName: \"flux-git-deploy\"
registry:
  automationInterval: 1m
" >./flux/flux.values.yml

try
(  #flux:
    # prepare for new flux deployment:
    [[ $(kubectl create namespace $namespace) ]]
    [[ $(kubectl -n $namespace create secret generic flux-git-deploy --from-file=identity=./flux/ssh-key/flux_rsa) ]] # путь параметризовать! /apply/flux/ssh-key/flux_rsa
    [[ $(kubectl apply -f https://raw.githubusercontent.com/fluxcd/helm-operator/master/deploy/crds.yaml) ]]
    echo "########################################"
    echo "## FLUX: safety-create.sh: well done ##"
    echo "########################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 