#!/bin/bash

namespace=$1

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

try
(  #flux:
    [[ $(kubectl get ns -l flux=enabled -o jsonpath={.items[*].metadata.name} | xargs -n 1 kubectl delete ns) ]]
    [[ $(kubectl get crd | grep -i flux | awk '{print $1}' | xargs -n 1 kubectl delete crd) ]]
    [[ $(kubectl delete deployment flux -n $namespace) ]]
    [[ $(kubectl delete deployment flux-memcached -n $namespace) ]]
    [[ $(kubectl delete deployment helm-operator -n $namespace) ]]
    [[ $(kubectl delete ClusterRole flux) ]]
    [[ $(kubectl delete ClusterRole helm-operator) ]]
    [[ $(kubectl delete ClusterRoleBinding flux) ]]
    [[ $(kubectl delete ClusterRoleBinding helm-operator) ]]
    [[ $(kubectl delete svc flux -n $namespace) ]]
    [[ $(kubectl delete svc flux-memcached -n $namespace) ]]
    [[ $(kubectl delete svc helm-operator -n $namespace) ]]
    [[ $(kubectl delete --cascade namespace $namespace) ]]
    [[ $(kubectl delete secret flux-git-deploy) ]]
    echo "########################################"
    echo "## FLUX: safety-delete.sh: well done ##"
    echo "########################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 