#!/bin/bash

namespace=$1

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}



try
(  
    [[ $(kubectl create namespace $namespace) ]]
    [[ $(istioctl operator init) ]]
    echo "#########################################"
    echo "## ISTIO: safety-prepare.sh: well done ##"
    echo "#########################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 


