#!/bin/bash


namespace=$1
istio_operator=$2

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}



try
(  
    # delete old istio components: 
    echo "istiooperators"
    [[ $(kubectl delete istiooperators.install.istio.io -n $namespace $istio_operator) ]]    
    echo "ns istio-operator"
    [[ $(kubectl delete ns istio-operator --grace-period=0 --force) ]]
    echo "istioctl manifest generate"
    [[ $(istioctl manifest generate | kubectl delete -f -) ]]
    echo "crd"
    [[ $(kubectl get crd | grep -i istio | awk '{print $1}' | xargs -n 1 kubectl delete crd) ]]
    echo "secrets"
    [[ $(kubectl get secrets --all-namespaces | grep "istio\." | awk '{print "kubectl delete secret -n "$1" "$2}' | bash -) ]]
    echo "MutatingWebhookConfiguration"
    [[ $(kubectl delete MutatingWebhookConfiguration istio-sidecar-injector) ]]
    echo "namespace"
    [[ $(kubectl delete --cascade namespace $namespace) ]]
    echo "########################################"
    echo "## ISTIO: safety-delete.sh: well done ##"
    echo "########################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 


