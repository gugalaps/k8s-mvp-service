#!/bin/bash
namespace=$1

function try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

try
(  #flagger:
    [[ $(kubectl delete deployment flagger -n $namespace) ]]
    [[ $(kubectl get crd | grep -i flagger | awk '{print $1}' | xargs -n 1 kubectl delete crd) ]]
    [[ $(kubectl delete ClusterRole flagger) ]] 
    [[ $(kubectl delete ClusterRoleBinding flagger) ]]
    [[ $(kubectl api-resources --namespaced=false | awk '{print $1}' | xargs -n 1 kubectl delete -l app.kubernetes.io/name=flagger) ]]
    [[ $(kubectl delete crd canaries.flagger.app) ]]
    echo "############################################"
    echo "## FLAGGER: safety-prepare.sh: well done ##"
    echo "############################################"
)

catch || {
    case $ex_code in
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code
        ;;
    esac
} 