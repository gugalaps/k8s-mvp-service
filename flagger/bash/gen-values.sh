#!/bin/bash

helm_release=$1
slack_url_api_info=$2
slack_url_api_warn=$3
slack_url_api_err=$4
slack_channel_info=$5
slack_channel_warn=$6
slack_channel_err=$7

echo "
helm_release: $helm_release
slack_url_api_info: $slack_url_api_info
slack_channel_info: $slack_channel_info
slack_url_api_warn: $slack_url_api_warn
slack_channel_warn: $slack_channel_warn
slack_url_api_err: $slack_url_api_err
slack_channel_err: $slack_channel_err
">./flagger/flagger-helm/values.yaml

chmod 775 ./flagger/flagger-helm/values.yaml